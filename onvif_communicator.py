#!/usr/bin/env python
# -*- coding: utf-8 -*-

from onvif_py3 import ONVIFCamera
import datetime
from onvif_py3.exceptions import ONVIFError
import logging
from time import sleep


class ONVIF_communicator():

    def __init__(self, ip='127.0.0.1', port=80,
                 login='admin', pwd='admin',
                 encrypt=False, debug=False):
        logger = logging.getLogger('ONVIF_communicator')
        if debug:
            main_log_lvl = logging.DEBUG
        else:
            main_log_lvl = logging.DEBUG
        logger.setLevel(main_log_lvl)
        ch = logging.StreamHandler()
        ch.setLevel(main_log_lvl)
        formatter = logging.Formatter('[%(levelname)s] - %(filename)s - %(funcName)s - %(asctime)s - %(message)s')
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        self.logger = logger

        self.cam = ONVIFCamera(ip, port, login, pwd, encrypt=encrypt)
        self.ip = ip
        self.media_service = self.cam.create_media_service()

    def get_current_ip(self):
        return self.ip

    def get_bitrate(self, stream_id=0):
        configurations_list = self.media_service.\
                                GetVideoEncoderConfigurations()
        video_encoder_configuration = configurations_list[stream_id]
        return video_encoder_configuration.RateControl.BitrateLimit

    def get_framerate(self, stream_id=0):
        configurations_list = self.media_service.\
                                GetVideoEncoderConfigurations()
        video_encoder_configuration = configurations_list[stream_id]
        return video_encoder_configuration.RateControl.FrameRateLimit

    def get_resolution(self, stream_id=0):
        configurations_list = self.media_service.\
                                GetVideoEncoderConfigurations()
        video_encoder_configuration = configurations_list[stream_id]
        return video_encoder_configuration.Resolution.Width,\
            video_encoder_configuration.Resolution.Height
    #===========================================================================
    # def set_bitrate(ip,port,bitrate,login='admin',pwd='admin',stream_id=0):
    # 
    #     mycam = ONVIFCamera(ip, port, login, pwd, encrypt=encrypt)
    #     # Get video sources
    #     media_service = mycam.create_media_service()
    # 
    #     profiles = media_service.GetProfiles()
    #     token = profiles[stream_id]._token
    #     # Get all video source configurations
    #     configurations_list = media_service.GetVideoEncoderConfigurations()
    # 
    #     # Use the first profile and Profiles have at least one
    #     video_encoder_configuration =configurations_list[stream_id] 
    #     options = media_service.GetVideoEncoderConfigurationOptions({'ProfileToken':token})
    #     if 'Extension' in options:
    #         min_bitrate = int(options.Extension.H264[0].BitrateRange[0].Min[0])
    #         max_bitrate = int(options.Extension.H264[0].BitrateRange[0].Max[0])
    #     else:
    #         print('We do not know availbale min and max bitrates')
    #         min_bitrate = 100
    #         max_bitrate = 10000
    # 
    #     if bitrate>int(min_bitrate) and bitrate<max_bitrate:
    #         video_encoder_configuration.RateControl.BitrateLimit=bitrate
    #         #print (video_encoder_configuration)
    #         #z=input()
    #         request = media_service.create_type('SetVideoEncoderConfiguration')
    #         request.Configuration = video_encoder_configuration
    #         request.ForcePersistence = True
    #         try:
    #             media_service.SetVideoEncoderConfiguration(request)
    #             return True
    #         except Exception as err:
    #             #print(err)
    #             return False
    #     else:
    #         raise ValueError('Wrong bitrate you try to set {}. Bitrate needed between {} and {} kbps'.format(bitrate,min_bitrate,max_bitrate))
    # 
    # 
    # def onvif_set_min_res(ip,port=80,login='admin',pwd='admin',stream_id=1):
    #     mycam = ONVIFCamera(ip, port, login, pwd, encrypt=encrypt)
    #     # Get video sources
    #     media_service = mycam.create_media_service()
    # 
    #     profiles = media_service.GetProfiles()
    #     token = profiles[stream_id]._token
    #     # Get all video source configurations
    #     configurations_list = media_service.GetVideoEncoderConfigurations()
    # 
    #     # Use the first profile and Profiles have at least one
    #     video_encoder_configuration = configurations_list[stream_id]
    #     options = media_service.GetVideoEncoderConfigurationOptions({'ProfileToken':token})
    # 
    #     min_width = options.H264.ResolutionsAvailable[0].Width
    #     min_height = options.H264.ResolutionsAvailable[0].Height
    #     for i in options.H264.ResolutionsAvailable:
    #         if i.Width < min_width:
    #             min_width = i.Width
    #             min_height = i.Height
    #     video_encoder_configuration.Resolution.Width = min_width
    #     video_encoder_configuration.Resolution.Height = min_height
    # 
    #     request = media_service.create_type('SetVideoEncoderConfiguration')
    #     request.Configuration = video_encoder_configuration
    #         # ForcePersistence is obsolete and should always be assumed to be True
    #     request.ForcePersistence = True
    # 
    #         # Set the video encoder configuration
    #     media_service.SetVideoEncoderConfiguration(request)
    # 
    # 

    def set_datetime(self, time=None, utc=False, utc_as_local=False, tz='CST-5', utc_and_local=False):
        self.logger.debug('Setting datetime')
        # Get system date and time
        dt = self.cam.devicemgmt.GetSystemDateAndTime()
        # dt.strftime(self.date, '%Y-%m-%dT%H:%M:%S.%f')[:-3]+'000Z'
        utcnow = datetime.datetime.utcnow()
        now = datetime.datetime.now()
        dt['DateTimeType'] = 'Manual'
        print(dt)
        #вот тут какая то фигня с часовыми поясами, каждая камера ставит в том формате
        #  в каком считает нужным.
        dt['TimeZone']['TZ'] = tz
        ## 'CST-5' #'WAUST-5'#'GM-5'#UTC+5
        if utc_as_local:
            self.logger.debug('Try to set utc_as_local datetime {}'.format(now))
            dt['UTCDateTime']['Date']['Year'] = datetime.datetime.strftime(now,'%Y')
            dt['UTCDateTime']['Date']['Month'] = datetime.datetime.strftime(now,'%m')
            dt['UTCDateTime']['Date']['Day'] = datetime.datetime.strftime(now,'%d')
            dt['UTCDateTime']['Time']['Hour'] = datetime.datetime.strftime(now,'%H')
            dt['UTCDateTime']['Time']['Minute'] = datetime.datetime.strftime(now,'%M')
            dt['UTCDateTime']['Time']['Second'] = datetime.datetime.strftime(now,'%S')
        elif utc:
            self.logger.debug('Try to set UTC datetime {}'.format(utcnow))
            dt['UTCDateTime']['Date']['Year'] = datetime.datetime.strftime(utcnow,'%Y')
            dt['UTCDateTime']['Date']['Month'] = datetime.datetime.strftime(utcnow,'%m')
            dt['UTCDateTime']['Date']['Day'] = datetime.datetime.strftime(utcnow,'%d')
            dt['UTCDateTime']['Time']['Hour'] = datetime.datetime.strftime(utcnow,'%H')
            dt['UTCDateTime']['Time']['Minute'] = datetime.datetime.strftime(utcnow,'%M')
            dt['UTCDateTime']['Time']['Second'] = datetime.datetime.strftime(utcnow,'%S')
        elif utc_and_local:
            self.logger.debug('Try to set utc_and_local datetime {}'.format(now))
            dt['LocalDateTime']['Date']['Year'] = datetime.datetime.strftime(now,'%Y')
            dt['LocalDateTime']['Date']['Month'] = datetime.datetime.strftime(now,'%m')
            dt['LocalDateTime']['Date']['Day'] = datetime.datetime.strftime(now,'%d')
            dt['LocalDateTime']['Time']['Hour'] = datetime.datetime.strftime(now,'%H')
            dt['LocalDateTime']['Time']['Minute'] = datetime.datetime.strftime(now,'%M')
            dt['LocalDateTime']['Time']['Second'] = datetime.datetime.strftime(now,'%S')
            dt['UTCDateTime']['Date']['Year'] = datetime.datetime.strftime(utcnow,'%Y')
            dt['UTCDateTime']['Date']['Month'] = datetime.datetime.strftime(utcnow,'%m')
            dt['UTCDateTime']['Date']['Day'] = datetime.datetime.strftime(utcnow,'%d')
            dt['UTCDateTime']['Time']['Hour'] = datetime.datetime.strftime(utcnow,'%H')
            dt['UTCDateTime']['Time']['Minute'] = datetime.datetime.strftime(utcnow,'%M')
            dt['UTCDateTime']['Time']['Second'] = datetime.datetime.strftime(utcnow,'%S')

        else:
            self.logger.debug('Try to set local datetime {}'.format(now))
            dt['LocalDateTime']['Date']['Year'] = datetime.datetime.strftime(now,'%Y')
            dt['LocalDateTime']['Date']['Month'] = datetime.datetime.strftime(now,'%m')
            dt['LocalDateTime']['Date']['Day'] = datetime.datetime.strftime(now,'%d')
            dt['LocalDateTime']['Time']['Hour'] = datetime.datetime.strftime(now,'%H')
            dt['LocalDateTime']['Time']['Minute'] = datetime.datetime.strftime(now,'%M')
            dt['LocalDateTime']['Time']['Second'] = datetime.datetime.strftime(now,'%S')
        self.logger.debug('Try to set - {}'.format(dt))
        self.cam.devicemgmt.SetSystemDateAndTime(dt)
        dt['DateTimeType'] = 'NTP'
        self.cam.devicemgmt.SetSystemDateAndTime(dt)
        return True


    def set_ntp(self, ntp_ip='81.30.199.66'):

        # Get system date and time
        dt = self.cam.devicemgmt.GetNTP()
        self.logger.debug(dt)
        # self.logger.info()
        if dt['NTPManual'][0]['Type'] != "IPv4":
            dt['NTPManual'][0]['Type'] = "IPv4"
            self.cam.devicemgmt.SetNTP(dt)


        # self.logger.info(dt['NTPManual'][0]['IPv4Address'])
        if dt['NTPManual'][0]['IPv4Address']!=ntp_ip:
            dt['NTPManual'][0]['IPv4Address']=ntp_ip
        self.cam.devicemgmt.SetNTP(dt)
        dt = self.cam.devicemgmt.GetSystemDateAndTime()
        if dt['DateTimeType']!='NTP':
            dt['DateTimeType']='NTP'
            dt['TimeZone']['TZ']='CST-5'#UTC+5    
        self.cam.devicemgmt.SetSystemDateAndTime(dt)

    def get_datetime(self):
        self.logger.debug('Getting datetime')
        dt = self.cam.devicemgmt.GetSystemDateAndTime()
        date = dt['LocalDateTime']['Date']
        time = dt['LocalDateTime']['Time']
        if self.logger.level == logging.DEBUG:
            print(dt)
        cam_time = datetime.datetime(year=date['Year'], month=date['Month'],
                                     day=date['Day'], hour=time['Hour'],
                                     minute=time['Minute'], second=time['Second'])

        return cam_time

    # 
    # def get_firmware_version(ip,port,login='admin',pwd='admin'):
    #     mycam = ONVIFCamera(ip, port, login, pwd,encrypt=encrypt)
    #     device_info = mycam.devicemgmt.GetDeviceInformation()
    #     return (device_info['FirmwareVersion'])
    #===========================================================================

    def get_device_info(self):
        '''
        returns dict with camera's model, serial number,
        firmware version, mac address
        '''
        device_info = {}
        info = self.cam.devicemgmt.GetDeviceInformation()
        device_info['model'] = info.Model
        device_info['sn'] = info.SerialNumber
        device_info['fw'] = info.FirmwareVersion
        # список доступных методов для камеры в тч максимально поддерживаемая
        # версия
        # device_info = mycam.devicemgmt.GetCapabilities()

        # список доступных сетевых интерфейсов в т.ч. мак адрес
        device_info['mac'] = self.cam.devicemgmt.GetNetworkInterfaces()[0].Info.HwAddress
        return (device_info)

    def get_users_list(self):
        try:
            result = []
            user_list = self.cam.devicemgmt.GetUsers()
        except Exception as e:
            print('Could not get users list', e)
            return False
        for i in user_list:
            if i['Username']:
                result.append(i['Username'])
        return (result)

    #===========================================================================
    # def add_user(ip,port,login='admin',pwd='admin',cam_username='tv',cam_pwd='qwer123',cam_userlvl='Operator'):
    #     if cam_userlvl not in ['Administrator', 'Operator', 'User', 'Anonymous', 'Extended']:
    #         return False
    #     mycam = ONVIFCamera(ip, port, login, pwd,encrypt=encrypt)
    #     user_params = mycam.devicemgmt.create_type('CreateUsers')
    #     user_params.User = {'Username':cam_username,'Password':cam_pwd,'UserLevel': cam_userlvl}
    #     try:
    #         mycam.devicemgmt.CreateUsers(user_params)
    #     except Exception as err:
    #         print(err)
    #         return False
    #     return True
    # 
    # 
    # def get_user_lvl(ip,port,login='admin',pwd='admin',cam_username='tv'):
    #     '''
    #     return lvl of user
    #     '''
    #     try:
    #         mycam = ONVIFCamera(ip, port, login, pwd,encrypt=encrypt)
    #         user_list = mycam.devicemgmt.GetUsers()
    #         result = None
    #     except Exception:
    #         print('Could not get user lvl')
    #         return False
    #     for i in user_list:
    #         if i['Username']==cam_username:
    #             result=i['UserLevel']
    #     return (result)
    # 
    # 
    # def update_user(ip,port,login='admin',pwd='admin',cam_username='tv',cam_pwd=None,cam_userlvl=None):
    #     '''
    # 
    #     '''
    #     if cam_userlvl is None:
    #         cam_userlvl=get_user_lvl(ip, port, login, pwd, cam_username)
    #         if cam_userlvl==False:
    #             print('')
    #             return False
    #     elif cam_userlvl not in ['Administrator', 'Operator', 'User', 'Anonymous', 'Extended']:
    #         return False 
    # 
    #     if cam_pwd is None:
    #         raise ValueError('We need password value')
    #     
    #     mycam = ONVIFCamera(ip, port, login, pwd,encrypt=encrypt)
    #     user_params = mycam.devicemgmt.create_type('SetUser')
    #     #print(user_params)
    #     
    #     user_params.User = {'Username':cam_username,'Password':cam_pwd,'UserLevel': cam_userlvl}
    #     #user_params={'Username':cam_username,'Password':cam_pwd,'UserLevel': cam_userlvl}
    #     #print(user_params)
    #     try:
    #         mycam.devicemgmt.SetUser(user_params)
    #     except Exception:
    #         print ('Could not update user')
    #         return False
    #     
    #     return True
    # 
    # 
    # def reboot(ip,port=8999,login='admin',pwd='admin'):
    #     ''' Здесь нужно сделать проверку а перестала ли пинговаться 
    #     камера ибо грин хоть и ребутается но нужный респонс не возвращает
    #     отсюда ONVIF timeout exception
    #     '''
    #     try:
    #         mycam = ONVIFCamera(ip, port, login, pwd, encrypt=encrypt)
    #         response = mycam.devicemgmt.SystemReboot()
    # 
    #     except ONVIFError as e:
    #         print (e)
    #         raise
    #         return False
    #     return True
    #===========================================================================

    def get_info_for_portal(self):
        result = {}
        result['Users'] = self.get_users_list()
        result['Info'] = self.get_device_info()
        result['Bitrate'] = self.get_bitrate()
        result['Framerate'] = self.get_framerate()
        result['Resolution'] = self.get_resolution()
        return result

if __name__ == '__main__':

    cams_list = [('10.129.247.22', 80, 'admin', 'admin')]
    #cams_list = [('192.168.136.96', 80, 'admin', '12345')]
    for i in cams_list:
        #qihan
        cam = ONVIF_communicator(ip=i[0], port=i[1], login=i[2], pwd=i[3], debug=True)
        print(cam.get_datetime())
        cam.set_datetime(utc=True, tz='GMT+05:00')
        print(ONVIF_communicator(ip=i[0], port=i[1], login=i[2], pwd=i[3], debug=True).get_datetime())


    #===========================================================================
    # print(get_info_for_portal(ip=CAM_HOST,pwd=CAM_PASS,port=CAM_PORT,encrypt=encrypt))
    # z=input()
    # for i in range(100):
    #     print(get_info_for_portal(ip=CAM_HOST,pwd=CAM_PASS,port=CAM_PORT,encrypt=encrypt))
    #===========================================================================
        
    #===========================================================================
    # print(update_user(ip='192.168.161.225',pwd='123456',port=2000,
    #                   cam_username='admin',cam_pwd='123456',cam_userlvl=None))
    #===========================================================================
    #set_bitrate(ip='81.30.177.206',pwd='rzyPCs',port=80,bitrate=4096)
    #set_bitrate(ip='10.129.247.102',login='',pwd='',port=80,bitrate=4096)
    #print('Finished')
    
    #print(reboot('10.0.16.66'))
    #print('ready')
    #===========================================================================
    # for i in range(66,96):
    #     try:
    #         set_ntp('10.0.16.'+str(i))
    #         onvif_set_min_res('10.0.16.'+str(i))
    #     except Exception as e:
    #         print ('Error  with default port at  10.0.16.'+str(i),e)
    #         try:
    #             set_ntp('10.0.16.'+str(i),port=80)
    #             onvif_set_min_res('10.0.16.'+str(i),port=80)
    #         except Exception as e:
    #             print ('Error  with 80 port at  10.0.16.'+str(i),e)
    #===========================================================================