from datetime import datetime as dt


class UTC:
    """
    Represents current UTC time.
    """
    def __init__(self, date=None):
        if date is None:
            date = dt.utcnow()
        self.date = date

    def __str__(self):
        '''
        make 2016-07-14T04:51:37.675000Z
        '''
        return dt.strftime(self.date, '%Y-%m-%dT%H:%M:%S.%f')[:-3]+'000Z'


if __name__ == '__main__':
    print(UTC())
