# -*- coding: utf-8 -
from suds.sax.element import Element
from onvif_py3.suds_utc import UTC
from suds.wsse import UsernameToken, wssens, wsuns
import base64
import hashlib


wspassd = ('Type',
           'http://docs.oasis-open.org/wss/2004/01/oasis-200401-\
        wss-username-token-profile-1.0#PasswordDigest')
wsenctype = ('EncodingType',
             'http://docs.oasis-open.org/wss/2004/01/oasis-200401-\
             wss-soap-message-security-1.0#Base64Binary')


class UsernameDigestToken(UsernameToken):
    '''
    patched class from suds-passworddigest

    '''

    def setcreated(self, *args, **kwargs):
        UsernameToken.setcreated(self, *args, **kwargs)
        self.created = str(UTC(self.created))

    def reset(self):
        self.nonce = None
        self.created = None

    def generate_digest(self):
        if self.nonce is None:
            self.setnonce()
        if self.created is None:
            self.setcreated()
        in_string = str(self.nonce) + str(self.created) + self.password
        in_string = in_string.encode(encoding="utf-8")

        sha1 = hashlib.sha1(in_string)
        digest = (base64.b64encode(sha1.digest())[:]).decode('utf-8')

        return digest

    def xml(self):
        """
        Get xml representation of the object.
        @return: The root node.
        @rtype: L{Element}
        """
        root = Element('UsernameToken', ns=wssens)

        u = Element('Username', ns=wssens)
        u.setText(self.username)
        root.append(u)

        p = Element('Password', ns=wssens)
        p.setText(self.generate_digest())
        p.set(wspassd[0], wspassd[1])
        root.append(p)

        n = Element('Nonce', ns=wssens)

        self.nonce = self.nonce.encode(encoding="utf-8", errors="strict")
        n.setText(base64.b64encode(self.nonce)[:-1])
        n.set(wsenctype[0], wsenctype[1])
        root.append(n)

        n = Element('Created', ns=wsuns)
        n.setText(self.created)
        root.append(n)

        self.reset()
        return root


if __name__ == '__main__':
    u = UsernameDigestToken(nonce='212cf7800d174ad75d29800e2addc6a4',
                            created='2016-07-14T09:36:51.443000Z',
                            password='12345')
